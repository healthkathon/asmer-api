<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-WebApp',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'WebApp\controllers',
    'modules' => [
        'v1' => [
            'class' => 'WebApp\modules\v1\Module',
            "defaultRoute" => "site/index"
        ],
    ],
    'components' => [
        'request' => [
            'class' => '\yii\web\Request',
            'enableCookieValidation' => false,
            'enableCsrfValidation'   => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                'multipart/form-data' => 'yii\web\MultipartFormDataParser'
            ]

        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-WebApp', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the WebApp
            'name' => 'advanced-WebApp',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => \yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'v1/add-vote/<name:\w+>' => 'v1/vote-data/hit',
                'v1/multi-vote/<name:\w+>/<total:\d+>' => 'v1/vote-data/multi-hit',
                [
                    'class' => 'yii\rest\UrlRule',
                    'pluralize' => false,
                    'controller' => [
                        "v1/screening",
                        "v1/screening-detail",
                        "v1/answer-type",
                        "v1/answer-value",
                        "v1/screening-user",
                        "v1/screening-user-detail",
                        
                        "v1/vote",
                        "v1/vote-items",
                        "v1/vote-data",
                        "v1/vote-result",
                        "v1/vote-result-date",
                        
                    //   '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                    ],
                    'extraPatterns' => [
                        'OPTIONS search' => 'options',
                        'OPTIONS view' => 'options',
                        'OPTIONS update' => 'options',
                        'OPTIONS data-per-equipment' => 'options',
                        'OPTIONS my' => 'my',
                        'OPTIONS hit' => 'options',
                        'OPTIONS running-excel-master' => 'options',

                    ]
                ]
            ],
        ],

    ],
    'params' => $params,
];
