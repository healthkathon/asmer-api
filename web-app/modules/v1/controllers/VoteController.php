<?php

namespace WebApp\modules\v1\controllers;


use common\components\UnguardActiveAuthController;

use common\models\Vote;
use common\models\VoteSearch;

date_default_timezone_set('Asia/Jakarta');


class VoteController extends UnguardActiveAuthController
{
    public $modelClass = Vote::class;

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new VoteSearch(); // lihat model search
        return $searchModel->search(\Yii::$app->request->queryParams);
    }
}
