<?php

namespace WebApp\modules\v1\controllers;

use app\models\AnswerType;
use app\models\AnswerValue;
use common\components\UnguardActiveAuthController;
use common\models\AnswerTypeSearch;
use common\models\AnswerValueSearch;
use common\models\Screening;
use common\models\ScreeningSearch;

class AnswerValueController extends UnguardActiveAuthController
{
    public $modelClass = AnswerValue::class;

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new AnswerValueSearch(); // lihat model search
        return $searchModel->search(\Yii::$app->request->queryParams);
    }
}
