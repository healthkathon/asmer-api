<?php

namespace mobile\modules\v1\controllers;

use app\components\AngularActiveAuthController;
use app\models\sap\SapCode;
use app\models\sap\SapCodeSearch;

class ScreeningController extends AngularActiveAuthController
{
    public $modelClass = SapCode::class;

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new SapCodeSearch(); // lihat model search
        return $searchModel->search(\Yii::$app->request->queryParams);
    }
}
