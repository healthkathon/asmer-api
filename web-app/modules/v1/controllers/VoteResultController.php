<?php

namespace WebApp\modules\v1\controllers;

use common\models\VoteResult;
use common\components\UnguardActiveAuthController;

use common\models\Vote;
use common\models\VoteResultSearch;
use common\models\VoteSearch;

date_default_timezone_set('Asia/Jakarta');

class VoteResultController extends UnguardActiveAuthController
{
    public $modelClass = VoteResult::class;

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new VoteResultSearch(); // lihat model search
        return $searchModel->search(\Yii::$app->request->queryParams);
    }
}
