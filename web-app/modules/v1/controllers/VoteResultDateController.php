<?php

namespace WebApp\modules\v1\controllers;

use common\models\VoteResult;
use common\components\UnguardActiveAuthController;

use common\models\Vote;
use common\models\VoteResultDate;
use common\models\VoteResultDateSearch;
use common\models\VoteResultSearch;
use common\models\VoteSearch;

date_default_timezone_set('Asia/Jakarta');


class VoteResultDateController extends UnguardActiveAuthController
{
    public $modelClass = VoteResultDate::class;

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new VoteResultDateSearch(); // lihat model search
        return $searchModel->search(\Yii::$app->request->queryParams);
    }
}
