<?php

namespace WebApp\modules\v1\controllers;


use common\components\UnguardActiveAuthController;

use common\models\Vote;
use common\models\VoteItems;
use common\models\VoteItemsSearch;
use common\models\VoteSearch;

date_default_timezone_set('Asia/Jakarta');


class VoteItemsController extends UnguardActiveAuthController
{
    public $modelClass = VoteItems::class;

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new VoteItemsSearch(); // lihat model search
        return $searchModel->search(\Yii::$app->request->queryParams);
    }
}
