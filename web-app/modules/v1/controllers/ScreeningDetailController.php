<?php

namespace WebApp\modules\v1\controllers;


use common\components\UnguardActiveAuthController;
use common\models\Screening;
use common\models\ScreeningDetail;
use common\models\ScreeningDetailSearch;
use common\models\ScreeningSearch;

class ScreeningDetailController extends UnguardActiveAuthController
{
    public $modelClass = ScreeningDetail::class;

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new ScreeningDetailSearch(); // lihat model search
        return $searchModel->search(\Yii::$app->request->queryParams);
    }


}
