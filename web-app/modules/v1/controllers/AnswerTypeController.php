<?php

namespace WebApp\modules\v1\controllers;

use app\models\AnswerType;
use common\components\UnguardActiveAuthController;
use common\models\AnswerTypeSearch;
use common\models\Screening;
use common\models\ScreeningSearch;

class AnswerTypeController extends UnguardActiveAuthController
{
    public $modelClass = AnswerType::class;

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new AnswerTypeSearch(); // lihat model search
        return $searchModel->search(\Yii::$app->request->queryParams);
    }
}
