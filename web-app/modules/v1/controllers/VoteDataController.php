<?php

namespace WebApp\modules\v1\controllers;

use common\models\VoteResult;
use common\components\UnguardActiveAuthController;

use common\models\Vote;
use common\models\VoteItems;
use common\models\VoteItemsSearch;
use common\models\VoteResultDate;
use common\models\VotesData;
use common\models\VotesDataSearch;
use common\models\VoteSearch;
use Yii;

date_default_timezone_set('Asia/Jakarta');

class VoteDataController extends UnguardActiveAuthController
{
    public $modelClass = VotesData::class;

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new VotesDataSearch(); // lihat model search
        return $searchModel->search(\Yii::$app->request->queryParams);
    }

    public function actionHit($name)
    {
        $item = VoteItems::find()->where([
            "alias" => $name
        ])->one();

        $result = VoteResult::find()->where([
            "alias" => $name
        ])->one();

        $resultDate = VoteResultDate::find()->where([
            "alias" => $name,
            "date" => date("Y-m-d")
        ])->one();

        // try {
        $transaction = Yii::$app->db->beginTransaction();
        if ($item) {
            $data = new VotesData();
            $data->vote_id = $item->vote_id;
            $data->candidate_id = $item->id;
            $data->timestamp = date("Y-m-d H:i:s");
            $data->alias = $item->alias;
            $data->save();
            if ($result) {
                $result->vote_count = $result->vote_count + 1;
                $result->save(false);
                $resultDate->vote_count = $resultDate->vote_count + 1;
                // echo "<pre>";
                // var_dump($resultDate);
                // echo "<pre>";
                // die();
                $resultDate->save(false);
            } else {
                $result = new VoteResult();
                $result->vote_id = $item->vote_id;
                $result->candidate_id = $item->id;
                $result->vote_count = 1;
                $result->alias = $item->alias;
                $result->save(false);
            }
        }

        // If everything is successful, commit the transaction
        $transaction->commit();
        // } catch (\Throwable $th) {
        //     //throw $th;
        //     $transaction->rollBack();
        // }

        return [
            "status" => true,
            "msg" => "single",
            "data" => $result,
            "data2" => $resultDate
        ];
    }

    public function actionMultiHit($name, $total)
    {
        // var_dump($total);
        // die();
        $item = VoteItems::find()->where([
            "alias" => $name
        ])->one();

        $result = VoteResult::find()->where([
            "alias" => $name
        ])->one();

        $resultDate = VoteResultDate::find()->where([
            "alias" => $name,
            "date" => date("Y-m-d")
        ])->one();

        // try {
        $transaction = Yii::$app->db->beginTransaction();
        if ($item) {
            $data = new VotesData();
            $data->vote_id = $item->vote_id;
            $data->candidate_id = $item->id;
            $data->alias = $item->alias;
            $data->total_vote = $total;
            $data->timestamp = date("Y-m-d H:i:s");
            // var_dump(date("Y-m-d H:i:s"));die();
            $data->save();
            if ($result) {
                $result->vote_count = $result->vote_count + $total;
                $result->save(false);
                $resultDate->vote_count = $resultDate->vote_count + $total;
                $resultDate->save(false);
            } else {
                $result = new VoteResult();
                $result->vote_id = $item->vote_id;
                $result->candidate_id = $item->id;
                $result->vote_count = $total;
                $result->alias = $item->alias;
                $result->save(false);
            }
        }

        // If everything is successful, commit the transaction
        $transaction->commit();
        // } catch (\Throwable $th) {
        //     //throw $th;
        //     $transaction->rollBack();
        // }

        return [
            "status" => true,
            "msg" => "multi",
            "data" => $result,
            "data2" => $resultDate
        ];
    }

    public function actionStart($vote_id)
    {
        $item = VoteItems::find()->where([
            "vote_id" => $vote_id
        ])->one();

        try {
            $transaction = Yii::$app->db->beginTransaction();
            if ($item) {
                $data = new VotesData();
                $data->vote_id = $item->vote_id;
                $data->candidate_id = $item->id;
                $data->alias = $item->alias;
                $data->save();
                $result = VoteResult::find()->where([
                    "alias" => $item->alias
                ])->one();
                if ($result) {
                } else {
                    $result = new VoteResult();
                    $result->vote_id = $item->vote_id;
                    $result->candidate_id = $item->id;
                    $result->vote_count = 0;
                    $result->alias = $item->alias;
                    $result->save(false);

                    $result = new VoteResultDate();
                    $result->vote_id = $item->vote_id;
                    $result->date = date("Y-m-d");
                    $result->candidate_id = $item->id;
                    $result->vote_count = 0;
                    $result->alias = $item->alias;
                    $result->save(false);
                }
            }

            // If everything is successful, commit the transaction
            $transaction->commit();
        } catch (\Throwable $th) {
            //throw $th;
            $transaction->rollBack();
        }

        return [
            "status" => true,
            "data" => $result
        ];
    }
}
