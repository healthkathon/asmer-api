<?php

namespace WebApp\modules\v1\controllers;


use common\components\UnguardActiveAuthController;
use common\models\Screening;
use common\models\ScreeningSearch;

class ScreeningController extends UnguardActiveAuthController
{
    public $modelClass = Screening::class;

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $searchModel = new ScreeningSearch(); // lihat model search
        return $searchModel->search(\Yii::$app->request->queryParams);
    }
}
