<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%m_screening}}`.
 */
class m230909_165900_create_m_screening_table extends Migration
{/**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('m_screening', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'code' => $this->string(100),
            'desc' => $this->text(),
            'answer_type_id' => $this->integer(),
            'check_duration' => $this->integer(),
            'total_question' => $this->integer(),
            'footnote' => $this->text(),
            'threshold' => $this->integer(),
            'point' => $this->integer(),
            'status_active' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'created_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp()->null()->defaultExpression('NULL ON UPDATE CURRENT_TIMESTAMP'),
            'deleted_by' => $this->integer(),
            'deleted_at' => $this->timestamp()->null()->defaultExpression('NULL'),
        ]);

        $this->createIndex(
            'idx-answer_type_id',
            'm_screening',
            'answer_type_id'
        );

        // $this->addForeignKey(
        //     'fk-answer_type_id',
        //     'm_screening',
        //     'answer_type_id',
        //     'm_answer_type',
        //     'id',
        //     'CASCADE'
        // );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        // $this->dropForeignKey('fk-answer_type_id', 'm_screening');
        // $this->dropIndex('idx-answer_type_id', 'm_screening');
        // $this->dropTable('m_screening');
    }
}