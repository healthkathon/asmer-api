<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%s_screening_user_detail}}`.
 */
class m230909_165904_create_s_screening_user_detail_table extends Migration
{/**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('s_screening_user_detail', [
            'id' => $this->primaryKey(),
            'screening_user_id' => $this->integer(),
            'screening_item_id' => $this->integer(),
            'screening_item_value' => $this->integer(),
            'status_active' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'created_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp()->null()->defaultExpression('NULL ON UPDATE CURRENT_TIMESTAMP'),
            'deleted_by' => $this->integer(),
            'deleted_at' => $this->timestamp()->null()->defaultExpression('NULL'),
        ]);

        $this->createIndex(
            'idx-screening_user_id',
            's_screening_user_detail',
            'screening_user_id'
        );

        $this->addForeignKey(
            'fk-screening_user_id',
            's_screening_user_detail',
            'screening_user_id',
            's_screening_user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-screening_item_id',
            's_screening_user_detail',
            'screening_item_id'
        );

        $this->addForeignKey(
            'fk-screening_item_id',
            's_screening_user_detail',
            'screening_item_id',
            'm_screening_detail',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropForeignKey('fk-screening_user_id', 's_screening_user_detail');
        $this->dropIndex('idx-screening_user_id', 's_screening_user_detail');

        $this->dropForeignKey('fk-screening_item_id', 's_screening_user_detail');
        $this->dropIndex('idx-screening_item_id', 's_screening_user_detail');

        $this->dropTable('s_screening_user_detail');
    }
}