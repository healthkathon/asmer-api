<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%m_answer_value}}`.
 */
class m230909_165858_create_m_answer_value_table extends Migration
{ /**
    * {@inheritdoc}
    */
   public function up()
   {
       $this->createTable('m_answer_value', [
           'id' => $this->primaryKey(),
           'answer_type_id' => $this->integer(),
           'name' => $this->string(100),
           'code' => $this->string(100),
           'value' => $this->integer(),
           'status_active' => $this->tinyInteger(1)->notNull()->defaultValue(1),
           'created_by' => $this->integer(),
           'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
           'updated_by' => $this->integer(),
           'updated_at' => $this->timestamp()->null()->defaultExpression('NULL ON UPDATE CURRENT_TIMESTAMP'),
           'deleted_by' => $this->integer(),
           'deleted_at' => $this->timestamp()->null()->defaultExpression('NULL'),
       ]);

       $this->createIndex(
           'idx-answer_type_id',
           'm_answer_value',
           'answer_type_id'
       );

       $this->addForeignKey(
           'fk-answer_type_id',
           'm_answer_value',
           'answer_type_id',
           'm_answer_type',
           'id',
           'CASCADE'
       );
   }

   /**
    * {@inheritdoc}
    */
   public function down()
   {
       $this->dropForeignKey('fk-answer_type_id', 'm_answer_value');
       $this->dropIndex('idx-answer_type_id', 'm_answer_value');
       $this->dropTable('m_answer_value');
   }
}