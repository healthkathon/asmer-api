<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%m_answer_type}}`.
 */
class m230909_165848_create_m_answer_type_table extends Migration
{ /**
    * {@inheritdoc}
    */
   public function up()
   {
       $this->createTable('m_answer_type', [
           'id' => $this->primaryKey(),
           'type' => $this->string(100),
           'status_active' => $this->tinyInteger(1)->notNull()->defaultValue(1),
           'created_by' => $this->integer(),
           'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
           'updated_by' => $this->integer(),
           'updated_at' => $this->timestamp()->null()->defaultExpression('NULL ON UPDATE CURRENT_TIMESTAMP'),
           'deleted_by' => $this->integer(),
           'deleted_at' => $this->timestamp()->null()->defaultExpression('NULL'),
       ]);
   }

   /**
    * {@inheritdoc}
    */
   public function down()
   {
       $this->dropTable('m_answer_type');
   }
}