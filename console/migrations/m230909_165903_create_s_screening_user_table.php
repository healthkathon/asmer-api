<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%s_screening_user}}`.
 */
class m230909_165903_create_s_screening_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('s_screening_user', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'screening_id' => $this->integer(),
            'dob' => $this->date(),
            'date' => $this->date(),
            'age' => $this->integer(),
            'phone' => $this->string(100),
            'result' => $this->integer(),
            'point_earned' => $this->integer(),
            'status_active' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'created_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp()->null()->defaultExpression('NULL ON UPDATE CURRENT_TIMESTAMP'),
            'deleted_by' => $this->integer(),
            'deleted_at' => $this->timestamp()->null()->defaultExpression('NULL'),
        ]);

        $this->createIndex(
            'idx-user_id',
            's_screening_user',
            'user_id'
        );

        $this->addForeignKey(
            'fk-user_id',
            's_screening_user',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-screening_id',
            's_screening_user',
            'screening_id'
        );

        $this->addForeignKey(
            'fk-screening_id',
            's_screening_user',
            'screening_id',
            'm_screening',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropForeignKey('fk-user_id', 's_screening_user');
        $this->dropIndex('idx-user_id', 's_screening_user');

        $this->dropForeignKey('fk-screening_id', 's_screening_user');
        $this->dropIndex('idx-screening_id', 's_screening_user');

        $this->dropTable('s_screening_user');
    }
}