<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%m_screening_detail}}`.
 */
class m230909_165901_create_m_screening_detail_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('m_screening_detail', [
            'id' => $this->primaryKey(),
            'screening_id' => $this->integer(),
            'order' => $this->integer(),
            'item_name' => $this->string(255),
            'answer_type_id' => $this->integer(),
            'status_active' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'created_by' => $this->integer(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_by' => $this->integer(),
            'updated_at' => $this->timestamp()->null()->defaultExpression('NULL ON UPDATE CURRENT_TIMESTAMP'),
            'deleted_by' => $this->integer(),
            'deleted_at' => $this->timestamp()->null()->defaultExpression('NULL'),
        ]);

        $this->createIndex(
            'idx-screening_id',
            'm_screening_detail',
            'screening_id'
        );

        // $this->addForeignKey(
        //     'fk-screening_id',
        //     'm_screening_detail',
        //     'screening_id',
        //     'm_screening',
        //     'id',
        //     'CASCADE'
        // );

        $this->createIndex(
            'idx-answer_type_id',
            'm_screening_detail',
            'answer_type_id'
        );

        // $this->addForeignKey(
        //     'fk-answer_type_id',
        //     'm_screening_detail',
        //     'answer_type_id',
        //     'm_answer_type',
        //     'id',
        //     'CASCADE'
        // );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropForeignKey('fk-screening_id', 'm_screening_detail');
        $this->dropIndex('idx-screening_id', 'm_screening_detail');

        $this->dropForeignKey('fk-answer_type_id', 'm_screening_detail');
        $this->dropIndex('idx-answer_type_id', 'm_screening_detail');

        $this->dropTable('m_screening_detail');
    }
}