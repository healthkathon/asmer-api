<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "s_screening_user".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $screening_id
 * @property string|null $dob
 * @property string|null $date
 * @property int|null $age
 * @property string|null $phone
 * @property int|null $result
 * @property int|null $point_earned
 * @property int $status_active
 * @property int|null $created_by
 * @property string $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 *
 * @property SScreeningUserDetail[] $sScreeningUserDetails
 * @property MScreening $screening
 * @property User $user
 */
class ScreeningUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 's_screening_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'screening_id', 'age', 'result', 'point_earned', 'status_active', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['dob', 'date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['phone'], 'string', 'max' => 100],
            [['screening_id'], 'exist', 'skipOnError' => true, 'targetClass' => MScreening::class, 'targetAttribute' => ['screening_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'screening_id' => 'Screening ID',
            'dob' => 'Dob',
            'date' => 'Date',
            'age' => 'Age',
            'phone' => 'Phone',
            'result' => 'Result',
            'point_earned' => 'Point Earned',
            'status_active' => 'Status Active',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
        ];
    }

    /**
     * Gets query for [[SScreeningUserDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSScreeningUserDetails()
    {
        return $this->hasMany(SScreeningUserDetail::class, ['screening_user_id' => 'id']);
    }

    /**
     * Gets query for [[Screening]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getScreening()
    {
        return $this->hasOne(Screening::class, ['id' => 'screening_id']);
    }

    public function getScreeningDetail()
    {
        return $this->hasMany(ScreeningUserDetail::class, ['screening_user_id' => 'id']);
    }


    public function extraFields()
    {
       return  ["screening", 'screeningDetail'];
    }

    // public function getAnswer()
    // {
    //     return $this->hasMany(AnswerValue::class, ['answer_type_id' => 'answer_type_id']);
    // }

    // public function getAnswerType()
    // {
    //     return $this->hasOne(AnswerType::class, ['id' => 'answer_type_id']);
    // }

    // public $answerTypeName;
    // public function fields()
    // {
    //     $fields = parent::fields();
    //     $this->answerTypeName = @$this->answerType->type;
        
    //     $fields['answer_name'] = 'answerTypeName';
        
    //     return $fields;
    // }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
