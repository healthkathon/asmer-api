<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "m_answer_type".
 *
 * @property int $id
 * @property string|null $type
 * @property int $status_active
 * @property int|null $created_by
 * @property string $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 *
 * @property MAnswerValue[] $mAnswerValues
 */
class AnswerType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_answer_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_active', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['type'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'status_active' => 'Status Active',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
        ];
    }

    /**
     * Gets query for [[MAnswerValues]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMAnswerValues()
    {
        return $this->hasMany(MAnswerValue::class, ['answer_type_id' => 'id']);
    }
}
