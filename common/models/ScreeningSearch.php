<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Screening;

/**
 * ScreeningSearch represents the model behind the search form of `common\models\Screening`.
 */
class ScreeningSearch extends Screening
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'answer_type_id', 'check_duration', 'total_question', 'threshold', 'point', 'status_active', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'code', 'desc', 'footnote', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Screening::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params, "");

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'answer_type_id' => $this->answer_type_id,
            'check_duration' => $this->check_duration,
            'total_question' => $this->total_question,
            'threshold' => $this->threshold,
            'point' => $this->point,
            'status_active' => 1,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'footnote', $this->footnote]);

        return $dataProvider;
    }
}
