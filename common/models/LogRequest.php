<?php
// protected/models/LogRequest.php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class LogRequest extends ActiveRecord
{
    public static function tableName()
    {
        return 'log_request';
    }

    public function rules()
    {
        return [
            [['url', 'method', 'source_ip'], 'required'],
            [['params', 'body', 'response'], 'safe'],
            [['status_code'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'URL',
            'method' => 'Method',
            'params' => 'Params',
            'body' => 'Body',
            'source_ip' => 'Source IP',
            'response' => 'Response',
            'status_code' => 'Status Code',
            'created_at' => 'Created At',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }
}
