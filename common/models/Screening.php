<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "m_screening".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $code
 * @property string|null $desc
 * @property int|null $answer_type_id
 * @property int|null $check_duration
 * @property int|null $total_question
 * @property string|null $footnote
 * @property int|null $threshold
 * @property int|null $point
 * @property int $status_active
 * @property int|null $created_by
 * @property string $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 *
 * @property SScreeningUser[] $sScreeningUsers
 */
class Screening extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_screening';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['desc', 'footnote'], 'string'],
            [['answer_type_id', 'check_duration', 'total_question', 'threshold', 'point', 'status_active', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'desc' => 'Desc',
            'answer_type_id' => 'Answer Type ID',
            'check_duration' => 'Check Duration',
            'total_question' => 'Total Question',
            'footnote' => 'Footnote',
            'threshold' => 'Threshold',
            'point' => 'Point',
            'status_active' => 'Status Active',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
        ];
    }

    /**
     * Gets query for [[SScreeningUsers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSScreeningUsers()
    {
        return $this->hasMany(SScreeningUser::class, ['screening_id' => 'id']);
    }


    public function extraFields()
    {
        return  ["answer", 'answerType'];
    }

    public function getAnswer()
    {
        return $this->hasMany(AnswerValue::class, ['answer_type_id' => 'answer_type_id']);
    }

    public function getAnswerType()
    {
        return $this->hasOne(AnswerType::class, ['id' => 'answer_type_id']);
    }

    public $answerTypeName;
    public function fields()
    {
        $fields = parent::fields();
        $this->answerTypeName = @$this->answerType->type;
        // var_dump($this->answerTypeName);
        // die();
        $fields['answer_name'] = 'answerTypeName';
        

        return $fields;
    }
}
