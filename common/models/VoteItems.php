<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vote_items".
 *
 * @property int $id
 * @property int|null $vote_id
 * @property string $name
 * @property string|null $description
 * @property string|null $image
 * @property int|null $status_active
 * @property string $created_at
 * @property int|null $created_by
 * @property string $updated_at
 * @property int|null $updated_by
 * @property string $deleted_at
 * @property int|null $deleted_by
 */
class VoteItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vote_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vote_id', 'status_active', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name'], 'required'],
            [['description'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vote_id' => 'Election ID',
            'name' => 'Name',
            'description' => 'Description',
            'image' => 'Image',
            'status_active' => 'Status Active',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }
}
