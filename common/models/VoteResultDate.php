<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "v_result_date".
 *
 * @property int $id
 * @property string|null $date
 * @property int|null $vote_id
 * @property int|null $candidate_id
 * @property string|null $alias
 * @property int|null $vote_count
 * @property int $status_active
 * @property string $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property string|null $deleted_at
 * @property int|null $deleted_by
 */
class VoteResultDate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vote_result_date';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['vote_id', 'candidate_id', 'vote_count', 'status_active', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['alias', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'vote_id' => 'Vote ID',
            'candidate_id' => 'Candidate ID',
            'alias' => 'Alias',
            'vote_count' => 'Vote Count',
            'status_active' => 'Status Active',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }
}
