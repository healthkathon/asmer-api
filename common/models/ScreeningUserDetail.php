<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "s_screening_user_detail".
 *
 * @property int $id
 * @property int|null $screening_user_id
 * @property int|null $screening_item_id
 * @property int|null $screening_item_value
 * @property int $status_active
 * @property int|null $created_by
 * @property string $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 *
 * @property MScreeningDetail $screeningItem
 * @property SScreeningUser $screeningUser
 */
class ScreeningUserDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 's_screening_user_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['screening_user_id', 'screening_item_id', 'screening_item_value', 'status_active', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['screening_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => MScreeningDetail::class, 'targetAttribute' => ['screening_item_id' => 'id']],
            [['screening_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => SScreeningUser::class, 'targetAttribute' => ['screening_user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'screening_user_id' => 'Screening User ID',
            'screening_item_id' => 'Screening Item ID',
            'screening_item_value' => 'Screening Item Value',
            'status_active' => 'Status Active',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
        ];
    }

    /**
     * Gets query for [[ScreeningItem]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getScreeningItem()
    {
        return $this->hasOne(ScreeningDetail::class, ['id' => 'screening_item_id']);
    }

    /**
     * Gets query for [[ScreeningUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getScreeningUser()
    {
        return $this->hasOne(SScreeningUser::class, ['id' => 'screening_user_id']);
    }


    public function getAnswerDetail()
    {
        return $this->hasOne(AnswerValue::class, ['id' => 'screening_item_value']);
    }


    public function extraFields()
    {
       return  ["screeningItem",  'answerDetail'];
    }
}
