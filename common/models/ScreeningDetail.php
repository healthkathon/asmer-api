<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "m_screening_detail".
 *
 * @property int $id
 * @property int|null $screening_id
 * @property int|null $order
 * @property string|null $item_name
 * @property int|null $answer_type_id
 * @property int $status_active
 * @property int|null $created_by
 * @property string $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 *
 * @property SScreeningUserDetail[] $sScreeningUserDetails
 */
class ScreeningDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_screening_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['screening_id', 'order', 'answer_type_id', 'status_active', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['item_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'screening_id' => 'Screening ID',
            'order' => 'Order',
            'item_name' => 'Item Name',
            'answer_type_id' => 'Answer Type ID',
            'status_active' => 'Status Active',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
        ];
    }

    public function extraFields()
    {
       return  ["answer", 'answerType'];
    }

    public function getAnswer()
    {
        return $this->hasMany(AnswerValue::class, ['answer_type_id' => 'answer_type_id']);
    }

    public function getAnswerType()
    {
        return $this->hasOne(AnswerType::class, ['id' => 'answer_type_id']);
    }

    public $answerTypeName;
    public function fields()
    {
        $fields = parent::fields();
        $this->answerTypeName = @$this->answerType->type;
        
        $fields['answer_name'] = 'answerTypeName';
        
        return $fields;
    }
}
