<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ResultDate;

/**
 * ResultDateSearch represents the model behind the search form of `common\models\ResultDate`.
 */
class VoteResultDateSearch extends VoteResultDate
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vote_id', 'candidate_id', 'vote_count', 'status_active', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['date', 'alias', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VoteResultDate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params, "");

        $check = VoteResultDate::find()->where([
            "vote_id" => $this->vote_id,
            "date"=>date("Y-m-d")
        ])->all();
        if (empty($check)) {
            $items = VoteItems::find()->where(["vote_id" => $this->vote_id])->all();
            foreach ($items as $key => $item) {
                $new = new VoteResultDate();
                $new->vote_id = $item->vote_id;
                $new->date = date("Y-m-d");
                $new->candidate_id = $item->id;
                $new->alias = $item->alias;
                $new->image = $item->image;
                $new->vote_count = 0;
                $new->save(false);
            }
        }

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => date("Y-m-d"),
            'vote_id' => $this->vote_id,
            'candidate_id' => $this->candidate_id,
            'vote_count' => $this->vote_count,
            'status_active' => 1,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'alias', $this->alias]);

        return $dataProvider;
    }
}
