<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "m_answer_value".
 *
 * @property int $id
 * @property int|null $answer_type_id
 * @property string|null $name
 * @property string|null $code
 * @property int|null $value
 * @property int $status_active
 * @property int|null $created_by
 * @property string $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 *
 * @property MAnswerType $answerType
 */
class AnswerValue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_answer_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['answer_type_id', 'value', 'status_active', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'code'], 'string', 'max' => 100],
            [['answer_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => MAnswerType::class, 'targetAttribute' => ['answer_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'answer_type_id' => 'Answer Type ID',
            'name' => 'Name',
            'code' => 'Code',
            'value' => 'Value',
            'status_active' => 'Status Active',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
        ];
    }

    /**
     * Gets query for [[AnswerType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnswerType()
    {
        return $this->hasOne(MAnswerType::class, ['id' => 'answer_type_id']);
    }
}
