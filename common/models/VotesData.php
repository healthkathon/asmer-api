<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "votes_data".
 *
 * @property int $id
 * @property int|null $vote_id
 * @property int|null $candidate_id
 * @property string $timestamp
 * @property int|null $status_active
 * @property string $created_at
 * @property int|null $created_by
 * @property string $updated_at
 * @property int|null $updated_by
 * @property string $deleted_at
 * @property int|null $deleted_by
 */
class VotesData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'votes_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vote_id', 'total_vote', 'candidate_id', 'status_active', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['timestamp', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vote_id' => 'Vote ID',
            'candidate_id' => 'Candidate ID',
            'timestamp' => 'Timestamp',
            'status_active' => 'Status Active',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }
}
