<?php
// protected/components/LogBehavior.php

namespace common\components;

use Yii;
use yii\base\Behavior;
use yii\web\Controller;
use common\models\LogRequest; // Adjust this namespace based on your model location

class LogBehavior extends Behavior
{
    public function events()
    {
        return [
            Controller::EVENT_AFTER_ACTION => 'afterAction',
        ];
    }

    public function afterAction($event)
    {
        $request = Yii::$app->request;
        $response = Yii::$app->response;

        $logModel = new LogRequest();
        $logModel->url = $request->getUrl();
        $logModel->method = $request->getMethod();
        $logModel->params = json_encode($request->getQueryParams());
        $logModel->body = $request->getRawBody();
        $logModel->source_ip = $request->getUserIP();
        $logModel->response = $response->content;
        $logModel->status_code = $response->statusCode;

        $logModel->save();
    }
}
